superbuild_add_project(itkvtkglue
  DEPENDS cxx11 itk paraview qt5 boost
  # Why this fails here? Not sure. But tell the project about Python if it's
  # enabled.
  DEPENDS_OPTIONAL python python2 python3
  CMAKE_ARGS
    # TODO: Fix this when ITK allows ITKVtkGlue to be built against
    #       an installed ITK instead of the build tree.
    -DITK_DIR:PATH=${itk_build_dir}
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
    # find_package(Boost) gets confused without this:
    -DBoost_USE_STATIC_LIBS:BOOL=OFF
)

## The ITKVtkGlue module uses the same configuration directory
## as ITK itself does, so there is no need to add it explicitly:
# superbuild_add_extra_cmake_args(
#   -DITKVTKGLUE_DIR:PATH=<INSTALL_DIR>/lib/cmake/ITK-5.1)

# TODO: remove when itkvtkglue can build against an ITK install
# tree. Copy targets file to install tree. Needed so
# aeva-session links on windows
superbuild_project_add_step(itkvtkglue
  COMMAND   "${CMAKE_COMMAND}"
            -Ditk_build_dir:PATH=${CMAKE_CURRENT_BINARY_DIR}
            -Dinstall_location:PATH=<INSTALL_DIR>
            -P "${CMAKE_CURRENT_LIST_DIR}/scripts/itkvtkglue.copytarget.cmake"
  DEPENDEES install
  COMMENT   "Copy ITKVtkGlueTargets.cmake to the sb install directory"
  WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}")
