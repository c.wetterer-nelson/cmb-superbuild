
set(_cmb_cmake_dir "${CMAKE_CURRENT_LIST_DIR}")
include(SuperbuildPackageMacros)

function (cmb_generate_package_bundle name)
  set(flag_args NO_DOC HAS_EXAMPLES HAS_WORKFLOWS)
  set(value_args SUFFIX DESCRIPTION CPACK_NAME PACKAGE_VERSION PACKAGE_NAME)
  set(multi_args APPLICATIONS)
  cmake_parse_arguments(bundle
    "${flag_args}"
    "${value_args}"
    "${multi_args}"
    ${ARGN})

  # Setup the extra arguments to pass to the CMake bundler
  set(cmb_configuration_variables)
  if (NOT bundle_NO_DOC)
    list(APPEND cmb_configuration_variables cmb_doc_dir)
    if (APPLE)
      list(APPEND cmb_configuration_variables cmb_doc_base_dir)
    endif ()
  endif ()
  if (bundle_HAS_WORKFLOWS)
    list(APPEND cmb_configuration_variables cmb_workflow_dir)
  endif ()
  if (bundle_HAS_EXAMPLES)
    list(APPEND cmb_configuration_variables cmb_example_dir)
  endif ()
  list(APPEND cmb_configuration_variables plugin_dir)

  if (NOT bundle_DESCRIPTION)
    set(bundle_DESCRIPTION "CMB ${name} application")
  endif ()

  if (NOT bundle_PACKAGE_NAME)
    set(bundle_PACKAGE_NAME "${name}")
  endif ()

  if (NOT bundle_CPACK_NAME)
    string(TOUPPER "${bundle_PACKAGE_NAME}" bundle_CPACK_NAME)
  endif ()

  if (NOT bundle_APPLICATIONS)
    set(bundle_APPLICATIONS "modelbuilder")
  endif ()

  if (NOT bundle_PACKAGE_VERSION)
    set(bundle_PACKAGE_VERSION "cmb")
  endif ()

  if (APPLE)
    set(cmb_doc_dir "share/cmb/doc")
    set(cmb_doc_base_dir "doc")
    set(cmb_workflow_dir "Contents/Resources/workflows")
    set(cmb_example_dir "examples")
    set(plugin_dir "lib")
    set(cmb_platform_bundle "cmb.bundle.apple")
  elseif (UNIX)
    set(cmb_doc_dir "share/cmb/doc")
    set(cmb_workflow_dir "share/cmb/workflows")
    set(cmb_example_dir "share/examples")
    set(plugin_dir "lib")
    set(cmb_platform_bundle "cmb.bundle.unix")
  elseif (WIN32)
    set(cmb_doc_dir "doc")
    set(cmb_workflow_dir "share/cmb/workflows")
    set(cmb_example_dir "examples")
    set(plugin_dir "bin")
    set(cmb_platform_bundle "cmb.bundle.windows")
  else ()
    message(FATAL_ERROR "unsupported platform")
  endif ()

  set("${name}_PACKAGE_SUFFIX" ${bundle_SUFFIX})
  superbuild_package_suffix(${name}_PACKAGE_SUFFIX)
  set(bundle_SUFFIX "${${name}_PACKAGE_SUFFIX}")

  set(cmb_platform_configuration_variables)
  foreach (variable IN LISTS cmb_configuration_variables)
    set(cmb_platform_configuration_variables "${cmb_platform_configuration_variables}set(\"${variable}\" \"${${variable}}\")\n")
  endforeach ()
  configure_file("${_cmb_cmake_dir}/package.bundle.cmake.in"
                 "${CMAKE_BINARY_DIR}/cpack/${name}/cmake/${name}.bundle.cmake"
                 @ONLY)
  list(PREPEND CMAKE_MODULE_PATH "${CMAKE_BINARY_DIR}/cpack/${name}/cmake")
  set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} PARENT_SCOPE)
endfunction()
