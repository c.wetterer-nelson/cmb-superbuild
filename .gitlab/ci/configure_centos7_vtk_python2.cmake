set(ENABLE_python2 ON CACHE BOOL "")
set(ENABLE_python3 OFF CACHE BOOL "")

set(numpy_SOURCE_SELECTION python2 CACHE STRING "")

include("${CMAKE_CURRENT_LIST_DIR}/configure_centos7_vtk.cmake")
