# Configure the vtkonly build.
set(ENABLE_cmb "OFF" CACHE BOOL "")
set(SUPERBUILD_PACKAGE_MODE "<none>" CACHE STRING "")
# matplotlib is not necessary, but this is as good a place as any to test it
set(ENABLE_matplotlib "ON" CACHE BOOL "")
set(ENABLE_paraview "OFF" CACHE BOOL "")
set(ENABLE_smtkresourcemanagerstate "OFF" CACHE BOOL "")
set(ENABLE_vtkonly "ON" CACHE BOOL "")

include("${CMAKE_CURRENT_LIST_DIR}/configure_centos7.cmake")
