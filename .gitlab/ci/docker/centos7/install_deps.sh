#!/bin/sh

# Install build requirements.
yum install -y \
    zlib-devel libcurl-devel python-devel \
    freeglut-devel glew-devel graphviz-devel libpng-devel \
    libxcb libxcb-devel libXt-devel xcb-util xcb-util-devel mesa-libGL-devel \
    libxkbcommon-devel libxkbcommon-x11-devel file mesa-dri-drivers chrpath \
    libicu-devel

# fontconfig installed as work-around for occt, should be fixed upstream
# see https://dev.opencascade.org/index.php?q=node/1295
yum install -y \
    fontconfig-devel

# Install EPEL
yum install -y \
    epel-release

# Install development tools
yum install -y \
    git-core \
    git-lfs

# Install toolchains.
yum install -y \
    centos-release-scl
yum install -y \
    devtoolset-7-gcc-c++ \
    devtoolset-7 \
    devtoolset-7-gcc

yum clean all
