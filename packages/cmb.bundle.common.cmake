# Consolidates platform independent stub for cmb.bundle.cmake files.

################################################################################
# Version information
################################################################################

include(cmb-version)
include(paraview-version)
include(smtk-version)

if (NOT DEFINED package_version_name)
  set(package_version_name cmb)
endif ()

################################################################################
# Package metadata
################################################################################

if (NOT DEFINED package_suffix)
  set(package_suffix "${${SUPERBUILD_PACKAGE_MODE}_PACKAGE_SUFFIX}")
endif ()

set(CPACK_PACKAGE_VENDOR "Kitware, Inc.")
set(CPACK_PACKAGE_VERSION_MAJOR ${${package_version_name}_version_major})
set(CPACK_PACKAGE_VERSION_MINOR ${${package_version_name}_version_minor})
set(CPACK_PACKAGE_VERSION_PATCH ${${package_version_name}_version_patch}${${package_version_name}_version_suffix})
if (package_suffix)
  set(CPACK_PACKAGE_VERSION_PATCH ${CPACK_PACKAGE_VERSION_PATCH}-${package_suffix})
endif ()

if (NOT DEFINED package_filename)
  set(package_filename "${${SUPERBUILD_PACKAGE_MODE}_PACKAGE_FILE_NAME}")
endif ()

if (package_filename)
  set(CPACK_PACKAGE_FILE_NAME "${package_filename}")
else ()
  set(CPACK_PACKAGE_FILE_NAME
    "${cmb_package_name}-${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")
endif ()

# Set the license file.
set(CPACK_RESOURCE_FILE_LICENSE
  "${superbuild_source_directory}/License.txt")

################################################################################
# Program selection
################################################################################

list(SORT cmb_programs_to_install)
list(REMOVE_DUPLICATES cmb_programs_to_install)

set(paraview_executables)
if (cmb_install_paraview_server)
  list(APPEND paraview_executables
    pvserver
    pvdataserver
    pvrenderserver)
endif ()
if (cmb_install_paraview_python)
  list(APPEND paraview_executables
    pvbatch
    pvpython)
endif ()

################################################################################
# Plugin file detection
################################################################################

function (extract_plugin_list varname pluginxml)
  file(STRINGS "${pluginxml}"
    plugin_lines
    REGEX "name=\"[A-Za-z0-9]+\"")
  set(plugin_names)
  foreach (plugin_line IN LISTS plugin_lines)
    string(REGEX REPLACE ".*name=\"\([A-Za-z0-9]+\)\".*" "\\1" plugin_name "${plugin_line}")
    list(APPEND plugin_names
      "${plugin_name}")
  endforeach ()
  set("${varname}"
    ${plugin_names}
    PARENT_SCOPE)
endfunction ()

foreach (project IN LISTS projects_with_plugins)
  set(found_files)
  foreach (plugin_file IN LISTS "${project}_plugin_files")
    file(GLOB_RECURSE files
      RELATIVE ${superbuild_install_location}
      "${superbuild_install_location}/${plugin_dir}/*/${plugin_file}")
    if (files)
      message(STATUS "Found: ${files}")
      list(APPEND found_files ${files})
    else ()
      message(SEND_ERROR "Could not file ParaView XML ${plugin_file} ${files}")
    endif ()
  endforeach ()
  set("${project}_plugin_files" ${found_files})
endforeach ()

set(all_plugin_files)
foreach (project IN LISTS projects_with_plugins)
  if (NOT ${project}_enabled)
    continue ()
  endif ()

  foreach (plugin_file IN LISTS "${project}_plugin_files")
    list(APPEND all_plugin_files "${plugin_file}")
    extract_plugin_list("plugins_${plugin_file}" "${superbuild_install_location}/${plugin_file}")

    if (DEFINED "${project}_plugin_omit")
      list(REMOVE_ITEM "plugins_${plugin_file}"
        ${${project}_plugin_omit})
    endif ()
  endforeach ()
endforeach ()

set(cmb_additional_libraries)
foreach (boost_lib IN LISTS BOOST_ADDITIONAL_LIBRARIES)
  list(APPEND cmb_additional_libraries boost_${boost_lib})
endforeach ()

################################################################################
# Qt5 plugins
################################################################################

if (qt5_enabled)
  include(qt5.functions)

  set(qt5_plugin_prefix)
  if (NOT WIN32)
    set(qt5_plugin_prefix "lib")
  endif ()

  # Add SVG support, so cmb can use paraview SVG icons.
  set(qt5_plugins
    iconengines/${qt5_plugin_prefix}qsvgicon
    imageformats/${qt5_plugin_prefix}qsvg
    sqldrivers/${qt5_plugin_prefix}qsqlite)

  if (WIN32)
    list(APPEND qt5_plugins
      platforms/qwindows)

    if (NOT qt5_version VERSION_LESS "5.10")
      list(APPEND qt5_plugins
        styles/qwindowsvistastyle)
    endif ()
  elseif (APPLE)
    list(APPEND qt5_plugins
      platforms/libqcocoa
      printsupport/libcocoaprintersupport)

    if (NOT qt5_version VERSION_LESS "5.10")
      list(APPEND qt5_plugins
        styles/libqmacstyle)
    endif ()
  elseif (UNIX)
    list(APPEND qt5_plugins
      platforms/libqxcb
      platforminputcontexts/libcomposeplatforminputcontextplugin
      xcbglintegrations/libqxcb-glx-integration)
  endif ()

  superbuild_install_qt5_plugin_paths(qt5_plugin_paths ${qt5_plugins})
else ()
  set(qt5_plugin_paths)
endif ()

################################################################################
# Data files
################################################################################

function (cmb_install_pdf project filename)
  if (${project}_enabled)
    install(
      FILES       "${superbuild_install_location}/doc/${filename}"
      DESTINATION "${cmb_doc_dir}"
      COMPONENT   superbuild)
  endif ()
endfunction ()

function (cmb_install_data project filename)
  if (${project}_enabled)
    install(
      FILES       "${superbuild_install_location}/examples/${filename}"
      DESTINATION "${cmb_example_dir}"
      COMPONENT   superbuild)
  endif ()
endfunction ()

function (cmb_install_extra_data)
  if (cmb_doc_dir)
    cmb_install_pdf(cmbusersguide "CMBUsersGuide.pdf")
    cmb_install_pdf(smtkusersguide "SMTKUsersGuide.pdf")
  endif ()
  if (cmb_example_dir)
    cmb_install_data(aevaexampledata "aeva-example.zip")
  endif()
endfunction ()
