foreach(program IN LISTS cmb_programs_to_install)
  set(additional_libraries)
  set(plugins)
  set(plugin_paths)

  # Install additional libraries in the app
  foreach(library IN LISTS cmb_additional_libraries)
    list(APPEND additional_libraries "${superbuild_install_location}/lib/lib${library}.dylib")
  endforeach()

  # Gather the list of plugins for this program
  if (EXISTS "${superbuild_install_location}/Applications/${program}.app/Contents/Resources/${program}.conf")
    file(READ "${superbuild_install_location}/Applications/${program}.app/Contents/Resources/${program}.conf" conf_contents)
    string(REGEX REPLACE "[^\n]*/" "../Plugins/" pkg_conf_contents "${conf_contents}")
    file(WRITE "${CMAKE_CURRENT_BINARY_DIR}/${program}.conf" "${pkg_conf_contents}")

    string(REPLACE "\n" ";" app_plugin_files "${conf_contents}")
    set(app_plugin_filenames)
    foreach (conf_line IN LISTS app_plugin_files)
      if (NOT conf_line)
        continue ()
      endif ()
      get_filename_component(plugin_filename "${conf_line}" NAME)
      list(APPEND app_plugin_filenames "${plugin_filename}")
    endforeach ()

    foreach (plugin_file IN LISTS all_plugin_files)
      # Check if the plugin file is recognized by the app.
      get_filename_component(plugin_file_name "${plugin_file}" NAME)
      if (NOT plugin_file_name IN_LIST app_plugin_filenames)
        continue ()
      endif ()

      # Get a list of plugins from this plugin file that need to be installed.
      get_filename_component(plugin_file_dir "${plugin_file}" DIRECTORY)
      foreach (plugin IN LISTS "plugins_${plugin_file}")
        list(APPEND plugin_paths "${superbuild_install_location}/${plugin_file_dir}/${plugin}/${plugin}.so")
        list(APPEND plugins "${plugin}")
      endforeach ()
    endforeach ()
  endif ()

  superbuild_apple_create_app(
    "\${CMAKE_INSTALL_PREFIX}"
    "${program}.app"
    "${superbuild_install_location}/Applications/${program}.app/Contents/MacOS/${program}"
    CLEAN
    PLUGINS ${plugin_paths}
    ADDITIONAL_LIBRARIES ${additional_libraries}
    SEARCH_DIRECTORIES
            "${superbuild_install_location}/lib")
  foreach (icon_filename MacIcon.icns pvIcon.icns modelbuilder.icns aeva.icns)
    set(icon_path "${superbuild_install_location}/Applications/${program}.app/Contents/Resources/${icon_filename}")
    if (EXISTS "${icon_path}")
      install(
        FILES       "${icon_path}"
        DESTINATION "${program}.app/Contents/Resources"
        COMPONENT   superbuild)
    endif ()
  endforeach ()
  install(
    FILES       "${superbuild_install_location}/Applications/${program}.app/Contents/Info.plist"
    DESTINATION "${program}.app/Contents"
    COMPONENT   superbuild)

  install(
    FILES       "${CMAKE_CURRENT_BINARY_DIR}/${program}.conf"
    DESTINATION "${program}.app/Contents/Resources/"
    COMPONENT   superbuild)
  foreach (app_plugin_file IN LISTS app_plugin_files)
    if (NOT app_plugin_file)
      continue ()
    endif ()
    if (IS_ABSOLUTE "${app_plugin_file}")
      set(plugin_source_path "${app_plugin_file}")
    else ()
      set(plugin_source_path "${superbuild_install_location}/Applications/${program}.app/Contents/Resources/${app_plugin_file}")
    endif ()
    install(
      FILES       "${plugin_source_path}"
      DESTINATION "${program}.app/Contents/Plugins/"
      COMPONENT   superbuild)
  endforeach ()

  foreach (executable IN LISTS paraview_executables)
    superbuild_apple_install_utility(
      "\${CMAKE_INSTALL_PREFIX}"
      "${program}.app"
      "${superbuild_install_location}/bin/${executable}"
      SEARCH_DIRECTORIES
              "${superbuild_install_location}/lib")
  endforeach ()

  install(CODE
    "file(MAKE_DIRECTORY \"\${CMAKE_INSTALL_PREFIX}/${program}.app/Contents/Resources\")
    file(WRITE \"\${CMAKE_INSTALL_PREFIX}/${program}.app/Contents/Resources/qt.conf\" \"[Paths]\nPlugins = Plugins\n\")"
    COMPONENT superbuild)

  if (python3_built_by_superbuild)
    include(python3.functions)
    superbuild_install_superbuild_python3(
      BUNDLE "${program}.app")
  endif ()

  file(GLOB egg_dirs
    "${superbuild_install_location}/lib/python${superbuild_python_version}/site-packages/*.egg/")
  superbuild_apple_install_python(
    "\${CMAKE_INSTALL_PREFIX}"
    "${program}.app"
    MODULES ${cmb_python_modules}
    MODULE_DIRECTORIES
            "${superbuild_install_location}/Applications/paraview.app/Contents/Python"
            "${superbuild_install_location}/lib/python${superbuild_python_version}/site-packages"
            ${egg_dirs}
    SEARCH_DIRECTORIES
            "${superbuild_install_location}/Applications/paraview.app/Contents/Libraries"
            "${superbuild_install_location}/lib")

  if (matplotlib_enabled)
    install(
      DIRECTORY   "${superbuild_install_location}/lib/python${superbuild_python_version}/site-packages/matplotlib/mpl-data/"
      DESTINATION "${program}.app/Contents/Python/matplotlib/mpl-data"
      COMPONENT   superbuild)
  endif ()

  if (pythonrequests_enabled)
    install(
      FILES       "${superbuild_install_location}/lib/python${superbuild_python_version}/site-packages/requests/cacert.pem"
      DESTINATION "${program}.app/Contents/Python/requests"
      COMPONENT   superbuild)
  endif ()

  if (paraviewweb_enabled)
    install(
      FILES       "${superbuild_install_location}/Applications/paraview.app/Contents/Python/paraview/web/defaultProxies.json"
      DESTINATION "${program}.app/Contents/Python/paraview/web"
      COMPONENT   "superbuild")
    install(
      DIRECTORY   "${superbuild_install_location}/share/paraview/web"
      DESTINATION "${program}.app/Contents/Resources"
      COMPONENT   "superbuild")
  endif ()

  foreach (qt5_plugin_path IN LISTS qt5_plugin_paths)
    get_filename_component(qt5_plugin_group "${qt5_plugin_path}" DIRECTORY)
    get_filename_component(qt5_plugin_group "${qt5_plugin_group}" NAME)

    superbuild_apple_install_module(
      "\${CMAKE_INSTALL_PREFIX}"
      "${program}.app"
      "${qt5_plugin_path}"
      "Contents/Plugins/${qt5_plugin_group}"
      SEARCH_DIRECTORIES  "${library_paths}")
  endforeach ()

  if (cmb_doc_base_dir)
    set(cmb_doc_dir "${program}.app/Contents/Resources/${cmb_doc_base_dir}")
  endif ()

  # Install PDF guides.
  cmb_install_extra_data()

  # Note: Ignoring the cmb_workflow_dir for apple and treating it as a flag
  if (cmb_workflow_dir)
    install(
      DIRECTORY   "${superbuild_install_location}/share/cmb/workflows/"
      DESTINATION ${program}.app/${cmb_workflow_dir}
      COMPONENT   superbuild)
  endif ()
endforeach ()

if (meshkit_enabled)
  foreach (meshkit_exe IN ITEMS coregen assygen)
    superbuild_apple_install_utility(
      "\${CMAKE_INSTALL_PREFIX}"
      "modelbuilder.app"
      "${superbuild_install_location}/bin/${meshkit_exe}"
      SEARCH_DIRECTORIES  "${library_paths}"
      FRAMEWORK_DEST      "Frameworks/meshkit"
      LIBRARY_DEST        "Libraries/meshkit")
  endforeach ()
endif ()
