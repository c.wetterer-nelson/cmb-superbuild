message(STATUS "Configuring modelbuilder package")
set_property(GLOBAL PROPERTY modelbuilder_REQUIRED_PROJECTS "cmb;smtk;cmbworkflows")
set_property(GLOBAL PROPERTY modelbuilder_EXCLUDE_PROJECTS "")

# Force this on for the modelbuilder mode
set(_superbuild_default_cmbworkflows ON)

include(CMBBundleMacros)
cmb_generate_package_bundle(modelbuilder
  HAS_EXAMPLES
  HAS_WORKFLOWS)
